import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../../services/client.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
  displayedColumns: string[];

  constructor(
    public clientService: ClientService
  ) { }

  ngOnInit(): void {
    this.displayedColumns = ['type', 'amount', 'detail', 'createdAt'];
  }

}
