import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { ClientService } from '../../../services/client.service';
import { AuthService } from '../../../services/auth.service';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

@Component({
  selector: 'app-charge',
  templateUrl: './charge.component.html',
  styleUrls: ['./charge.component.scss']
})
export class ChargeComponent implements OnInit {
  chargeForm;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private clientService: ClientService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.chargeForm = new FormGroup({
      amount: new FormControl('', [
        Validators.required,
        Validators.min(1)
      ])
    });
  }

  // tslint:disable-next-line:typedef
  get amount() {
    return this.chargeForm.get('amount');
  }

  charge(formDirective: FormGroupDirective): void {
    const amount = {
      amount: this.chargeForm.value.amount
    };
    this.clientService.chargeAmout(amount, this.authService.getIdClient(), 'Carga', 'Carga de saldo.').subscribe(
      response => {
        this.snackBar.open('Monto cargado', 'cerrar', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        formDirective.resetForm();
        this.chargeForm.reset();
      },
      error => {
        this.snackBar.open('Error al cargar. Vualva a intentar.', 'cerrar', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        console.error(error);
      }
    );
  }

}
