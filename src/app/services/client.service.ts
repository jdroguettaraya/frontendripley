import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { headerBase } from '../constanst/config.constants';
import { HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { MatTableDataSource } from '@angular/material/table';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  public clientData;
  public dataTable = new BehaviorSubject([]);

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  getClientData(): void {
    let headerGet = {};

    const baseUrl = environment.mid + 'account/getClientData';
    headerGet = headerBase;
    headerGet['x-access-token'] = this.authService.getAccessToken();

    const header = {
      headers: new HttpHeaders(headerGet),
    };

    this.http.get(baseUrl, header).subscribe(
      response => {
        this.clientData = response;
        console.log(this.clientData);
        this.dataTable.next(this.clientData.transactions);
      },
      error => {
        this.authService.logout();
      });
  }

  chargeAmout(value, id, type, msg): Observable<any>  {
    let headerGet = {};

    return new Observable(obs => {
      const baseUrl = environment.mid + 'account/chargeMoney/' + id;
      headerGet = headerBase;
      headerGet['x-access-token'] = this.authService.getAccessToken();

      const header = {
        headers: new HttpHeaders(headerGet),
      };

      this.http.put(baseUrl, value, header).subscribe(
        response => {
          this.pushInfoTransaction(id, type, value.amount, msg);
          obs.next();
        },
        error => {
          obs.error();
        });
    });
  }

  withdrawAmout(value, type, msg): Observable<any>  {
    let headerGet = {};

    return new Observable(obs => {
      const baseUrl = environment.mid + 'account/drawMoney';
      headerGet = headerBase;
      headerGet['x-access-token'] = this.authService.getAccessToken();

      const header = {
        headers: new HttpHeaders(headerGet),
      };

      const id = this.clientData._id;

      this.http.put(baseUrl, value, header).subscribe(
        response => {
          this.pushInfoTransaction(id, type, value.amount, msg);
          obs.next();
        },
        error => {
          obs.error(error);
        });
    });
  }

  transfer(rut, amount): Observable<any>  {
    let headerGet = {};
    let msgPush = '';

    return new Observable(obs => {
      headerGet = headerBase;
      headerGet['x-access-token'] = this.authService.getAccessToken();
      const header = {
        headers: new HttpHeaders(headerGet),
      };
      const baseUrl = environment.mid + 'account/getIdByRut?rut=' + rut;
      this.http.get(baseUrl, header).subscribe(
        response => {
          msgPush = 'Transferencia a: ' + this.getUsername(response);
          this.withdrawAmout(amount, 'Transferencia realizada', msgPush).subscribe(
            resp => {
              const userId = this.getIdByUser(response);
              msgPush = 'Transferencia de: ' + this.clientData.username;
              this.chargeAmout(amount, userId, 'Transferencia recibida',  msgPush).subscribe(
                res => {
                  obs.next('Transferencia Realizada.');
                },
                error => {
                  obs.error('No se pudo cargar el monto. Pongase en contacto con su ejecutivo');
                }
              );
            },
            error => {
              obs.error('No cuentas con el monto suficiente.');
            }
          );
        },
        error => {
          obs.error('Cuenta de destino no encontrada.');
        });
    });
  }

  getIdByUser(data): string {
    return data.id;
  }

  getUsername(data): string {
    return data.username;
  }

  pushInfoTransaction(id, type, amount, detail): void {
    let headerGet = {};

    const baseUrl = environment.mid + 'account/pushInfoTransaction';
    headerGet = headerBase;
    headerGet['x-access-token'] = this.authService.getAccessToken();

    const header = {
      headers: new HttpHeaders(headerGet),
    };

    const data = {
      id,
      type,
      amount,
      detail
    };

    this.http.post(baseUrl, data, header).subscribe(
      reponse => {
        this.getClientData();
      }, error => {
        this.getClientData();
      }
    );
  }
}
