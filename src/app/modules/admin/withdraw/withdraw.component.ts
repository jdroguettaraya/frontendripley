import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { ClientService } from '../../../services/client.service';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})
export class WithdrawComponent implements OnInit {
  withdramForm;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private clientService: ClientService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.withdramForm = new FormGroup({
      amount: new FormControl('', [
        Validators.required,
        Validators.min(1)
      ])
    });
  }

  // tslint:disable-next-line:typedef
  get amount() {
    return this.withdramForm.get('amount');
  }

  withdraw(formDirective: FormGroupDirective): void {
    const amount = {
      amount: this.withdramForm.value.amount
    };
    this.clientService.withdrawAmout(amount, 'Retiro', 'Retiro de fondos').subscribe(
      response => {
        this.snackBar.open('Monto retirado', 'cerrar', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        formDirective.resetForm();
        this.withdramForm.reset();
      },
      err => {
        this.snackBar.open(err.error.message, 'cerrar', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        console.error(err);
      }
    );
  }

}
