import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormGroupDirective } from '@angular/forms';
import { ClientService } from '../../../services/client.service';
import { AuthService } from '../../../services/auth.service';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {
  transferForm;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private clientService: ClientService,
    private authService: AuthService,
    private snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
    this.transferForm = new FormGroup({
      rut: new FormControl('', [
        Validators.required
      ]),
      amount: new FormControl('', [
        Validators.required,
        Validators.min(1)
      ])
    });
  }

  // tslint:disable-next-line:typedef
  get rut() {
    return this.transferForm.get('rut');
  }
  // tslint:disable-next-line:typedef
  get amount() {
    return this.transferForm.get('amount');
  }

  transfer(formDirective: FormGroupDirective): void {
    const amount = {
      amount: this.transferForm.value.amount
    };
    this.clientService.transfer(this.transferForm.value.rut, amount).subscribe(
      response => {
        this.snackBar.open(response, 'cerrar', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        formDirective.resetForm();
        this.transferForm.reset();
      },
      error => {
        this.snackBar.open(error, 'cerrar', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        console.error(error);
      }
    );
  }

}
