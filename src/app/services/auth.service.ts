import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { headerBase } from '../constanst/config.constants';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token;
  private isLogged = false;

  constructor(
    private http: HttpClient,
    public router: Router,
  ) { }

  login(data): Observable<any>{
    const baseUrl = environment.mid + 'auth/signin';

    const header = {
      headers: new HttpHeaders(headerBase),
    };

    return new Observable(obs => {
      this.http.post(baseUrl, data, header).subscribe(
        response => {
          this.token = response;
          this.isLogged = true;
          obs.next();
        },
        error => {
          this.token = null;
          this.isLogged = false;
          obs.error(error);
        });
    });
  }

  register(data): Observable<any>{
    const baseUrl = environment.mid + 'auth/signup';

    const header = {
      headers: new HttpHeaders(headerBase),
    };

    return new Observable(obs => {
      this.http.post(baseUrl, data, header).subscribe(
        response => {
          obs.next();
        },
        error => {
          obs.error(error);
        });
    });
  }

  isloggedIn(): boolean {
    if (!this.token || !this.isLogged) {
      return false;
    } else {
      return true;
    }
  }

  getAccessToken(): string {
    return (this.token && this.token.accessToken) ? this.token.accessToken : '';
  }

  getIdClient(): string {
    return (this.token && this.token.id) ? this.token.id : '';
  }

  logout(): void {
    this.token = null;
    this.isLogged = false;
    this.router.navigate(['/']);
  }
}
