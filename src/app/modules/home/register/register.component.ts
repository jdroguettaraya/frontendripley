import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  response;
  registerForm;

  constructor(
    public dialogRef: MatDialogRef<RegisterComponent>,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      username: new FormControl('', Validators.required),
      rut: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }
  // tslint:disable-next-line:typedef
  get username() {
    return this.registerForm.get('username');
  }
  // tslint:disable-next-line:typedef
  get rut() {
    return this.registerForm.get('rut');
  }
  // tslint:disable-next-line:typedef
  get email() {
    return this.registerForm.get('email');
  }
  // tslint:disable-next-line:typedef
  get password() {
    return this.registerForm.get('password');
  }

  register(): void {
    console.log(this.registerForm.value);

    let rut = this.registerForm.value.rut;
    rut = rut.replaceAll(/\./gi, '');
    rut = rut.replaceAll(/-/gi, '');
    this.registerForm.value.rut = rut;

    this.authService.register(this.registerForm.value).subscribe(
      response => {
        this.response = {
          msg: 'Nueva cuenta registrada'
        };
        this.dialogRef.close(this.response);
      },
      err => {
        this.response = {
          msg: err.error.message
        };
        this.dialogRef.close(this.response);
      });
  }

  cancel(): void {
    this.dialogRef.close();
  }

}
