import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ChargeComponent } from './charge/charge.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { TransferComponent } from './transfer/transfer.component';
import { TransactionComponent } from './transaction/transaction.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent, children: [
      {
        path: 'cargar', component: ChargeComponent
      }, {
        path: 'retirar', component: WithdrawComponent
      }, {
        path: 'transferir', component: TransferComponent
      }, {
        path: 'movimientos', component: TransactionComponent
      }, {
        path: '', redirectTo: 'cargar', pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
