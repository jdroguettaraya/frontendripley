import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  constructor(
    private authService: AuthService,
    public clientService: ClientService
  ) { }

  ngOnInit(): void {
    const token = this.authService.getAccessToken();

    this.clientService.getClientData();
  }

  logout(): void {
    this.authService.logout();
  }

}
