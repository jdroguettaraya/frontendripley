import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { RegisterComponent } from './register/register.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  loginForm;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private authService: AuthService,
    public router: Router,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      rut: new FormControl('', Validators.required),
      pass: new FormControl('', Validators.required),
    });
  }

  // tslint:disable-next-line:typedef
  get rut() {
    return this.loginForm.get('rut');
  }

  // tslint:disable-next-line:typedef
  get pass() {
    return this.loginForm.get('pass');
  }

  onSubmit(): void {
    let rut = this.loginForm.value.rut;
    rut = rut.replaceAll(/\./gi, '');
    rut = rut.replaceAll(/-/gi, '');
    const login = {
      rut,
      password: this.loginForm.value.pass
    };

    this.authService.login(login).subscribe(
      response => {
        this.router.navigate(['/sesion']);
      },
      error => {
        this.snackBar.open('RUT o Clave no validos', 'cerrar', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        console.error(error);
      });
  }

  register(): void {
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '70%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.snackBar.open(result.msg, 'cerrar', {
          duration: 5000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });
  }
}
